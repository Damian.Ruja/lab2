package INF101.lab2;


import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    int MaxCapacity = 20;
    List<FridgeItem> FridgeItems = new ArrayList<FridgeItem>();

    @Override
    public int nItemsInFridge() {

        return this.FridgeItems.size();
    }

    @Override
    public int totalSize() {
        
        return MaxCapacity;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if(nItemsInFridge()<20){
            FridgeItems.add(item);
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if(nItemsInFridge()>0){
            FridgeItems.remove(item);
        }
        else{
            throw new NoSuchElementException();
        }
        
    }

    @Override
    public void emptyFridge() {
        FridgeItems.clear();
        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredStuff = new ArrayList<FridgeItem>();

        for(FridgeItem item: FridgeItems){

            if (item.hasExpired()==false){
                expiredStuff.add(item);

            }
            

        }
        FridgeItems.removeAll(expiredStuff);
        return FridgeItems;
        

        


        
    }
    
}
